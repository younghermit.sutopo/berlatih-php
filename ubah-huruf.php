<?php
function ubah_huruf($str)
{
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $output = "";
    for ($i=0; $i < strlen($str); $i++){
        $position = strrpos($abjad, $str[$i]);
        $output .=substr($abjad, $position + 1, 1);
    }
    return $output;
}

// TEST CASES
echo "ubah huruf wow  :" .ubah_huruf('wow') . "<br>"; // xpx
echo "ubah huruf developer  :" .ubah_huruf('developer'). "<br>"; // efwfmpqfs
echo "ubah huruf laravel  :" .ubah_huruf('laravel'). "<br>"; // mbsbwfm
echo "ubah huruf keren  :" .ubah_huruf('keren'). "<br>"; // lfsfo
echo "ubah huruf semangat  :" .ubah_huruf('semangat'). "<br>"; // tfnbohbu

?>