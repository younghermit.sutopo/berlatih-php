<?php
function tentukan_nilai($number)
{
    $output = "";
    if ($number >= 85 && $number <100) {
        $output .= "Sangat Baik";
    } else if ($number >=75 && $number <85) {
        $output .= "Baik";
    } else if ($number >=60 && $number <75) {
        $output .= "Cukup";
    } else {$output .= "Kurang";
    }
    return $output;

}

//TEST CASES
echo "nilai 98 :" . tentukan_nilai(98). "<br>"; //Sangat Baik
echo "nilai 76 :" . tentukan_nilai(76). "<br>"; //Baik
echo "nilai 67 :" . tentukan_nilai(67). "<br>"; //Cukup
echo "nilai 43 :" . tentukan_nilai(43). "<br>"; //Kurang

?>